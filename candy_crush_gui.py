import sys
import random
import math
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QGraphicsScene, QGraphicsView, QGraphicsPolygonItem
from PyQt5.QtGui import QBrush, QPolygonF, QPen
from PyQt5.QtCore import Qt
'''
STARE:
 Candy Crush - pola hexagonalne. Po naciśnięciu przycisku "Start" na scenie pojawia się plansza, gdzie każdy "cukierek" to 
 osobny QGrpahicsItem oraz pozostałe przyciski interfejsu zostają odblokowane. 
 Plansza pozbawioną jest jakichkolwiek "trójek". Gracz ma możliwość zmieniania "cukierków" w 6 kierunkach za pomocą
 odpowiednich przycisków w interfejsie (buttony 1-6, gdzie każdy z przycisków to odpowiednia krawędź wzdłuż której nastąpi zamiana). 
 Aktualnie wybrane pole podświetlane jest czarną obwódką.
 Przechodzenie pomiędzy polami realizuje się za pomocą strzałek w interfejsie aplikacji. 
 Po każdym ruchu (zmianie "cukierków" miejscami) sprawdzana jest ilość "trójek"na planszy. 
 Jeżeli jest jakaś "trójka" to zostaje ona usunięta a graczowi zostaje przyznany punkt za każdą "trójkę"
 (jeżeli jest "czwórka"- 2pkt, "piątka"- 3pkt itd...). 
 W miejsce usuniętych "cukierków" spadają te z góry.
 Na samym szczycie planszy dodawane są nowe "cukierki". Może się zdarzyć tak, że nowe "cukierki", bądź te spadajce stworzą
 kolejną "trójkę" i proces się powtórzy. Wtedy także dodawane są odpowiednie punkty dla gracza, jednak
 wizualnie gracz tego nie doświadczy. Zdobyte punkty wyświetlane są w labelach obok odpowiedniego gracza. 
 Przycisk "Nowa gra" wczytuje nową planszę oraz resetuje punkty obu graczy. Przycisk wyjście zamyka aplikację.
 NOWE:
 Dodano opcję gry z botem wybieraną radiobuttonem. Po każdorazowym wyborze, należy wcisnąć przycisk Start, aby uaktualnić
 wybór, w przeciwnym wypadku ciągle będziemy grać w poprzednim trybie. Zmieniono opcję gry na dwóch graczy. Teraz każdy
 gracz ma jeden ruch (jedną zmianę cukierków miejscami), po wykonaniu ruchu punkty za następny ruch będą przyznanwane
 drugiemu graczowi i tak na zmianę. Czyli ruchy gracz1 -> gracz2 -> gracz1 -> gracz2....
 W trybie "vs Bot" po każdym ruchu gracza bot wybiera pierwszy, najlepszy możliwy ruch (najwięcej punktów do uzyskania)
 i wykonuje go, za co są mu przydzielane odpowiednie punkty(tak samo jak w przypadku ruchu gracza, podczas ruchu bota
 odpowiednie cukierki znikają a w ich miejsce spadają te z góry, które mogą stowrzyć kolejną kombinację i punkty nadal
 będą przyznawane dla bota). Gracz dostaje uaktualnioną mapę po ruchu bota.
 A odpowiednie punkty za daną kolejkę są widoczne po lewej stronie aplikacji. Punkty oczywiście sumują się.
 Domyślnie po odpaleniu aplikacji wybrany jest tryb zwykły - dla dwóch graczy.
'''


class Candy(QGraphicsPolygonItem):
    def __init__(self, x_ctr, y_ctr, radius, color):
        self.points = []
        self.pen = QPen(Qt.white)
        for i in range(6):
            x = x_ctr + radius * math.cos((i % 6) * 2 * math.pi / 6)
            y = y_ctr + radius * math.sin((i % 6) * 2 * math.pi / 6)
            point = QtCore.QPoint(int(x), int(y))
            self.points.append(point)
        self.polygon = QPolygonF(self.points)
        super().__init__(self.polygon)
        self.setBrush(color)
        self.setPen(self.pen)


class Board:
    def __init__(self):
        self.height = 15
        self.width = 20
        self.radius = 25
        self.candies = []
        self.colors = [QBrush(Qt.red), QBrush(Qt.green), QBrush(Qt.cyan), QBrush(Qt.blue), QBrush(Qt.magenta), QBrush(Qt.gray), QBrush(Qt.yellow)]
        self.selectedPen = QPen(Qt.black, 4)
        self.selectedIdx = 0
        self.idxToRemove = []
        self.points1 = 0
        self.points2 = 0

        for i in range(self.height):
            for j in range(self.width):
                x_ctr = 3 / 2 * self.radius * j
                if j % 2 == 0:
                    y_ctr = self.radius * math.sqrt(3) * i
                else:

                    y_ctr = self.radius * math.sqrt(3) / 2 * (2*i+1)
                candy = Candy(x_ctr, y_ctr, self.radius, self.colors[random.randint(0, 6)])
                self.candies.append(candy)
        remove, _ = self.check()
        while not remove:
            self.newCandies()
            remove, _ = self.check()
        self.candies[self.selectedIdx].setPen(self.selectedPen)

    def check(self):
        check = True
        row = 0
        self.idxToRemove = []

        for i in range(len(self.candies)):
            if i % self.width == 0:
                row += 1

            if i % 2 == 0:
                if (i < row * self.width - 2 and row != self.height and self.candies[i].brush() ==
                        self.candies[i + 1].brush() == self.candies[i + 2 + self.width].brush()):
                    check = False
                    self.idxToRemove.append(i)
                    self.idxToRemove.append(i + 1)
                    self.idxToRemove.append(i + 2 + self.width)
                if (i > (row - 1) * self.width + 1 and row != self.height and self.candies[i].brush() ==
                        self.candies[i - 1].brush() == self.candies[i - 2 + self.width].brush()):
                    check = False
                    self.idxToRemove.append(i)
                    self.idxToRemove.append(i - 1)
                    self.idxToRemove.append(i - 2 + self.width)
            else:
                if (i < row * self.width - 2 and row != self.height and self.candies[i].brush() ==
                        self.candies[i + 1 + self.width].brush() == self.candies[i + 2 + self.width].brush()):
                    check = False
                    self.idxToRemove.append(i)
                    self.idxToRemove.append(i + 1 + self.width)
                    self.idxToRemove.append(i + 2 + self.width)
                if (i > (row - 1) * self.width + 1 and row != self.height and self.candies[i].brush() ==
                        self.candies[i - 1 + self.width].brush() == self.candies[i - 2 + self.width].brush()):
                    check = False
                    self.idxToRemove.append(i)
                    self.idxToRemove.append(i - 1 + self.width)
                    self.idxToRemove.append(i - 2 + self.width)

            if row < self.height - 1 and (
                    self.candies[i].brush() == self.candies[i + self.width].brush() == self.candies[
                i + 2 * self.width].brush()):
                check = False
                self.idxToRemove.append(i)
                self.idxToRemove.append(i + self.width)
                self.idxToRemove.append(i + 2 * self.width)
        points = len(self.idxToRemove) / 3
        return check, int(points)

    def newCandies(self):
        self.idxToRemove = list(dict.fromkeys(self.idxToRemove))
        for i in range(len(self.idxToRemove)):
            while self.idxToRemove[i] >= self.width:
                buf = self.candies[self.idxToRemove[i]].brush()
                self.candies[self.idxToRemove[i]].setBrush(self.candies[self.idxToRemove[i] - self.width].brush())
                self.candies[self.idxToRemove[i] - self.width].setBrush(buf)
                self.idxToRemove[i] -= self.width
            self.candies[self.idxToRemove[i]].setBrush(self.colors[random.randint(0, 6)])

    def newBoard(self):
        self.selectedIdx = 0
        self.points1 = 0
        self.points2 = 0

        for i in self.candies:
            i.setBrush(self.colors[random.randint(0, 6)])
            i.setPen(i.pen)
        remove, _ = self.check()
        while not remove:
            self.newCandies()
            remove, _ = self.check()
        self.candies[self.selectedIdx].setPen(self.selectedPen)

    def changeSelected(self, idx1, idx2):
        self.candies[idx1].setPen(self.candies[idx2].pen)
        self.candies[idx2].setPen(self.selectedPen)


class Agent:
    def __init__(self):
        self.score = 0

    def changeField(self, board, direction):
        change = False
        prevIdx = board.selectedIdx

        if board.selectedIdx < board.width and direction == 1:
            pass
        elif board.selectedIdx % board.width == 0 and direction == 4:
            pass
        elif (board.selectedIdx-(board.width-1)) % board.width == 0 and direction == 2:
            pass
        elif board.selectedIdx >= board.width*(board.height-1) and direction == 3:
            pass
        else:
            change = True
            if direction == 1:
                board.selectedIdx -= board.width
            elif direction == 2:
                board.selectedIdx += 1
            elif direction == 3:
                board.selectedIdx += board.width
            else:
                board.selectedIdx -= 1

        if change:
            board.changeSelected(prevIdx, board.selectedIdx)

    def move(self, board, edgeIdx, bot, checkVsBot):
        checkMove = False
        candy1_idx = board.selectedIdx
        candy2_idx = candy1_idx

        if candy1_idx % 2 == 0:
            if candy1_idx < board.width and edgeIdx in [1, 2, 6]:
                pass
            elif candy1_idx%board.width == 0 and edgeIdx in [5, 6]:
                pass
            elif candy1_idx >= board.width*(board.height-1) and edgeIdx == 4:
                pass
            else:
                checkMove = True
                if edgeIdx == 1:
                    candy2_idx = candy1_idx - board.width
                elif edgeIdx == 2:
                    candy2_idx = candy1_idx - board.width + 1
                elif edgeIdx == 3:
                    candy2_idx = candy1_idx + 1
                elif edgeIdx == 4:
                    candy2_idx = candy1_idx + board.width
                elif edgeIdx == 5:
                    candy2_idx = candy1_idx - 1
                elif edgeIdx == 6:
                    candy2_idx = candy1_idx - board.width - 1
        else:
            if candy1_idx < board.width and edgeIdx == 1:
                pass
            elif (candy1_idx-(board.width-1)) % board.width == 0 and edgeIdx in [2, 3]:
                pass
            elif candy1_idx >= board.width*(board.height-1) and edgeIdx in [3, 4, 5]:
                pass
            else:
                checkMove = True
                if edgeIdx == 1:
                    candy2_idx = candy1_idx - board.width
                elif edgeIdx == 2:
                    candy2_idx = candy1_idx + 1
                elif edgeIdx == 3:
                    candy2_idx = candy1_idx + board.width + 1
                elif edgeIdx == 4:
                    candy2_idx = candy1_idx + board.width
                elif edgeIdx == 5:
                    candy2_idx = candy1_idx + board.width - 1
                elif edgeIdx == 6:
                    candy2_idx = candy1_idx - 1

        if checkMove:
            buf = board.candies[candy1_idx].brush()
            board.candies[candy1_idx].setBrush(board.candies[candy2_idx].brush())
            board.candies[candy2_idx].setBrush(buf)

            remove, p = board.check()
            self.score += p
            while not remove:
                board.newCandies()
                remove, p = board.check()
                self.score += p

        if checkVsBot:
            bot.selectBestMove(board)

    def newGame(self):
        self.score = 0


class Bot:
    def __init__(self):
        self.score = 0
        #self.selectedPen = QPen(Qt.darkRed, 4)

    def selectBestMove(self, board):
        currentPoints = 0
        bestCandyIdx = 0
        bestEdgeIdx = 1
        for i in range(len(board.candies)):
            for j in [1, 2, 3]:
                self.switch(i, j, board)
                points = self.testMove(board)
                self.switch(i, j, board)
                if points > currentPoints:
                    currentPoints = points
                    bestCandyIdx = i
                    bestEdgeIdx = j
        self.bestMove(bestCandyIdx, bestEdgeIdx, board)



    def switch(self, candyIdx, edgeIdx, board):
        checkMove = False
        candy1_idx = candyIdx
        candy2_idx = candy1_idx

        if candy1_idx % 2 == 0:
            if candy1_idx < board.width and edgeIdx in [1, 2]:
                pass
            else:
                checkMove = True
                if edgeIdx == 1:
                    candy2_idx = candy1_idx - board.width
                elif edgeIdx == 2:
                    candy2_idx = candy1_idx - board.width + 1
                elif edgeIdx == 3:
                    candy2_idx = candy1_idx + 1
                elif edgeIdx == 4:
                    candy2_idx = candy1_idx + board.width
                elif edgeIdx == 5:
                    candy2_idx = candy1_idx - 1
                elif edgeIdx == 6:
                    candy2_idx = candy1_idx - board.width - 1
        else:
            if candy1_idx < board.width and edgeIdx == 1:
                pass
            elif (candy1_idx - (board.width - 1)) % board.width == 0 and edgeIdx in [2, 3]:
                pass
            elif candy1_idx >= board.width * (board.height - 1) and edgeIdx == 3:
                pass
            else:
                checkMove = True
                if edgeIdx == 1:
                    candy2_idx = candy1_idx - board.width
                elif edgeIdx == 2:
                    candy2_idx = candy1_idx + 1
                elif edgeIdx == 3:
                    candy2_idx = candy1_idx + board.width + 1
                elif edgeIdx == 4:
                    candy2_idx = candy1_idx + board.width
                elif edgeIdx == 5:
                    candy2_idx = candy1_idx + board.width - 1
                elif edgeIdx == 6:
                    candy2_idx = candy1_idx - 1

        if checkMove:
            buf = board.candies[candy1_idx].brush()
            board.candies[candy1_idx].setBrush(board.candies[candy2_idx].brush())
            board.candies[candy2_idx].setBrush(buf)

    def testMove(self, board):
        row = 0
        idxToRemove = []

        for i in range(len(board.candies)):
            if i % board.width == 0:
                row += 1

            if i % 2 == 0:
                if (i < row * board.width - 2 and row != board.height and board.candies[i].brush() ==
                        board.candies[i + 1].brush() == board.candies[i + 2 + board.width].brush()):
                    idxToRemove.append(i)
                    idxToRemove.append(i + 1)
                    idxToRemove.append(i + 2 + board.width)
                if (i > (row - 1) * board.width + 1 and row != board.height and board.candies[i].brush() ==
                        board.candies[i - 1].brush() == board.candies[i - 2 + board.width].brush()):
                    idxToRemove.append(i)
                    idxToRemove.append(i - 1)
                    idxToRemove.append(i - 2 + board.width)
            else:
                if (i < row * board.width - 2 and row != board.height and board.candies[i].brush() ==
                        board.candies[i + 1 + board.width].brush() == board.candies[i + 2 + board.width].brush()):
                    idxToRemove.append(i)
                    idxToRemove.append(i + 1 + board.width)
                    idxToRemove.append(i + 2 + board.width)
                if (i > (row - 1) * board.width + 1 and row != board.height and board.candies[i].brush() ==
                        board.candies[i - 1 + board.width].brush() == board.candies[i - 2 + board.width].brush()):
                    idxToRemove.append(i)
                    idxToRemove.append(i - 1 + board.width)
                    idxToRemove.append(i - 2 + board.width)

            if row < board.height - 1 and (
                    board.candies[i].brush() == board.candies[i + board.width].brush() == board.candies[
                i + 2 * board.width].brush()):
                idxToRemove.append(i)
                idxToRemove.append(i + board.width)
                idxToRemove.append(i + 2 * board.width)
        points = len(idxToRemove) / 3
        return int(points)

    def bestMove(self, candyIdx, edgeIdx, board):
        checkMove = False
        candy1_idx = candyIdx
        candy2_idx = candy1_idx

        if candy1_idx % 2 == 0:
            if candy1_idx < board.width and edgeIdx in [1, 2]:
                pass
            else:
                checkMove = True
                if edgeIdx == 1:
                    candy2_idx = candy1_idx - board.width
                elif edgeIdx == 2:
                    candy2_idx = candy1_idx - board.width + 1
                elif edgeIdx == 3:
                    candy2_idx = candy1_idx + 1
                elif edgeIdx == 4:
                    candy2_idx = candy1_idx + board.width
                elif edgeIdx == 5:
                    candy2_idx = candy1_idx - 1
                elif edgeIdx == 6:
                    candy2_idx = candy1_idx - board.width - 1
        else:
            if candy1_idx < board.width and edgeIdx == 1:
                pass
            elif (candy1_idx - (board.width - 1)) % board.width == 0 and edgeIdx in [2, 3]:
                pass
            elif candy1_idx >= board.width * (board.height - 1) and edgeIdx == 3:
                pass
            else:
                checkMove = True
                if edgeIdx == 1:
                    candy2_idx = candy1_idx - board.width
                elif edgeIdx == 2:
                    candy2_idx = candy1_idx + 1
                elif edgeIdx == 3:
                    candy2_idx = candy1_idx + board.width + 1
                elif edgeIdx == 4:
                    candy2_idx = candy1_idx + board.width
                elif edgeIdx == 5:
                    candy2_idx = candy1_idx + board.width - 1
                elif edgeIdx == 6:
                    candy2_idx = candy1_idx - 1

        if checkMove:
            #board.candies[candy1_idx].setPen(self.selectedPen)
            #board.candies[candy2_idx].setPen(self.selectedPen)
            buf = board.candies[candy1_idx].brush()
            board.candies[candy1_idx].setBrush(board.candies[candy2_idx].brush())
            board.candies[candy2_idx].setBrush(buf)

            remove, p = board.check()
            self.score += p
            while not remove:
                board.newCandies()
                remove, p = board.check()
                self.score += p

    def newGame(self):
        self.score = 0


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1200, 800)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.startBtn = QtWidgets.QPushButton(self.centralwidget)
        self.startBtn.setGeometry(QtCore.QRect(20, 20, 131, 51))
        self.startBtn.setObjectName("startBtn")
        self.newGameBtn = QtWidgets.QPushButton(self.centralwidget)
        self.newGameBtn.setGeometry(QtCore.QRect(20, 80, 131, 51))
        self.newGameBtn.setObjectName("newGameBtn")
        self.exitBtn = QtWidgets.QPushButton(self.centralwidget)
        self.exitBtn.setGeometry(QtCore.QRect(20, 140, 131, 51))
        self.exitBtn.setObjectName("exitBtn")
        self.edge1Btn = QtWidgets.QPushButton(self.centralwidget)
        self.edge1Btn.setGeometry(QtCore.QRect(220, 200, 61, 31))
        self.edge1Btn.setObjectName("edge1Btn")
        self.edge6Btn = QtWidgets.QPushButton(self.centralwidget)
        self.edge6Btn.setGeometry(QtCore.QRect(160, 240, 61, 31))
        self.edge6Btn.setObjectName("edge6Btn")
        self.edge2Btn = QtWidgets.QPushButton(self.centralwidget)
        self.edge2Btn.setGeometry(QtCore.QRect(280, 240, 61, 31))
        self.edge2Btn.setObjectName("edge2Btn")
        self.edge3Btn = QtWidgets.QPushButton(self.centralwidget)
        self.edge3Btn.setGeometry(QtCore.QRect(280, 280, 61, 31))
        self.edge3Btn.setObjectName("edge3Btn")
        self.edge4Btn = QtWidgets.QPushButton(self.centralwidget)
        self.edge4Btn.setGeometry(QtCore.QRect(220, 320, 61, 31))
        self.edge4Btn.setObjectName("edge4Btn")
        self.edge5Btn = QtWidgets.QPushButton(self.centralwidget)
        self.edge5Btn.setGeometry(QtCore.QRect(160, 280, 61, 31))
        self.edge5Btn.setObjectName("edge5Btn")
        self.upArrowBtn = QtWidgets.QToolButton(self.centralwidget)
        self.upArrowBtn.setGeometry(QtCore.QRect(230, 50, 41, 41))
        self.upArrowBtn.setArrowType(QtCore.Qt.UpArrow)
        self.upArrowBtn.setObjectName("upArrowBtn")
        self.rightArrowBtn = QtWidgets.QToolButton(self.centralwidget)
        self.rightArrowBtn.setGeometry(QtCore.QRect(270, 90, 41, 41))
        self.rightArrowBtn.setArrowType(QtCore.Qt.RightArrow)
        self.rightArrowBtn.setObjectName("rightArrowBtn")
        self.leftArrowBtn = QtWidgets.QToolButton(self.centralwidget)
        self.leftArrowBtn.setGeometry(QtCore.QRect(190, 90, 41, 41))
        self.leftArrowBtn.setArrowType(QtCore.Qt.LeftArrow)
        self.leftArrowBtn.setObjectName("leftArrowBtn")
        self.downArrowBtn = QtWidgets.QToolButton(self.centralwidget)
        self.downArrowBtn.setGeometry(QtCore.QRect(230, 130, 41, 41))
        self.downArrowBtn.setArrowType(QtCore.Qt.DownArrow)
        self.downArrowBtn.setObjectName("downArrowBtn")
        self.normalGame_radioBtn = QtWidgets.QRadioButton(self.centralwidget)
        self.normalGame_radioBtn.setGeometry(QtCore.QRect(30, 400, 310, 30))
        self.normalGame_radioBtn.setFont(QtGui.QFont('SansSerif', 14))
        self.normalGame_radioBtn.setObjectName("player1_radioBtn")
        self.vsBot_radioBtn = QtWidgets.QRadioButton(self.centralwidget)
        self.vsBot_radioBtn.setGeometry(QtCore.QRect(30, 450, 310, 30))
        self.vsBot_radioBtn.setFont(QtGui.QFont('SansSerif', 14))
        self.vsBot_radioBtn.setObjectName("player2_radioBtn")
        self.player1Label = QtWidgets.QLabel(self.centralwidget)
        self.player1Label.setGeometry(QtCore.QRect(30, 500, 330, 30))
        self.player1Label.setFont(QtGui.QFont('SansSerif', 14))
        self.player1Label.setObjectName("player1Label")
        self.player2Label = QtWidgets.QLabel(self.centralwidget)
        self.player2Label.setGeometry(QtCore.QRect(30, 550, 330, 30))
        self.player2Label.setFont(QtGui.QFont('SansSerif', 14))
        self.player2Label.setObjectName("player2Label")
        self.scene = QGraphicsScene(self.centralwidget)
        self.view = QGraphicsView(self.scene, self.centralwidget)
        self.view.setGeometry(400, 50, 780, 700)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 432, 26))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.board = Board()
        self.agent1 = Agent()
        self.agent2 = Agent()
        self.bot = Bot()
        self.selectedAgent = self.agent1
        self.checkVsBot = False
        self.startBtn.clicked.connect(self.onStart)
        self.startBtn.clicked.connect(self.board.newBoard)
        self.startBtn.clicked.connect(self.agent1.newGame)
        self.startBtn.clicked.connect(self.agent2.newGame)
        self.startBtn.clicked.connect(self.bot.newGame)
        self.startBtn.clicked.connect(self.onMove)
        self.exitBtn.clicked.connect(sys.exit)
        self.normalGame_radioBtn.setChecked(True)

    def onMove(self):
        self.player1Label.setText("Gracz 1 zdobyte punkty: " + str(self.agent1.score))
        if self.checkVsBot:
            self.player2Label.setText("BOT zdobyte punkty: " + str(self.bot.score))
        else:
            self.player2Label.setText("Gracz 2 zdobyte punkty: " + str(self.agent2.score))

    def selectAgent1(self):
        self.selectedAgent = self.agent1

    def chagneAgent(self):
        if self.selectedAgent == self.agent1:
            self.selectedAgent = self.agent2
        else:
            self.selectedAgent = self.agent1

    def onStart(self):
        try:
            self.upArrowBtn.clicked.disconnect()
            self.rightArrowBtn.clicked.disconnect()
            self.downArrowBtn.clicked.disconnect()
            self.leftArrowBtn.clicked.disconnect()
            self.edge1Btn.clicked.disconnect()
            self.edge2Btn.clicked.disconnect()
            self.edge3Btn.clicked.disconnect()
            self.edge4Btn.clicked.disconnect()
            self.edge5Btn.clicked.disconnect()
            self.edge6Btn.clicked.disconnect()
            self.newGameBtn.clicked.disconnect()
        except: pass
        for i in self.board.candies:
            self.scene.addItem(i)
        self.upArrowBtn.clicked.connect(lambda: self.selectedAgent.changeField(self.board, 1))
        self.rightArrowBtn.clicked.connect(lambda: self.selectedAgent.changeField(self.board, 2))
        self.downArrowBtn.clicked.connect(lambda: self.selectedAgent.changeField(self.board, 3))
        self.leftArrowBtn.clicked.connect(lambda: self.selectedAgent.changeField(self.board, 4))
        self.edge1Btn.clicked.connect(lambda: self.selectedAgent.move(self.board, 1, self.bot, self.checkVsBot))
        self.edge1Btn.clicked.connect(self.onMove)
        self.edge2Btn.clicked.connect(lambda: self.selectedAgent.move(self.board, 2, self.bot, self.checkVsBot))
        self.edge2Btn.clicked.connect(self.onMove)
        self.edge3Btn.clicked.connect(lambda: self.selectedAgent.move(self.board, 3, self.bot, self.checkVsBot))
        self.edge3Btn.clicked.connect(self.onMove)
        self.edge4Btn.clicked.connect(lambda: self.selectedAgent.move(self.board, 4, self.bot, self.checkVsBot))
        self.edge4Btn.clicked.connect(self.onMove)
        self.edge5Btn.clicked.connect(lambda: self.selectedAgent.move(self.board, 5, self.bot, self.checkVsBot))
        self.edge5Btn.clicked.connect(self.onMove)
        self.edge6Btn.clicked.connect(lambda: self.selectedAgent.move(self.board, 6, self.bot, self.checkVsBot))
        self.edge6Btn.clicked.connect(self.onMove)
        self.newGameBtn.clicked.connect(self.board.newBoard)
        self.newGameBtn.clicked.connect(self.agent1.newGame)
        self.newGameBtn.clicked.connect(self.agent2.newGame)
        self.newGameBtn.clicked.connect(self.bot.newGame)
        self.newGameBtn.clicked.connect(self.onMove)
        self.newGameBtn.clicked.connect(self.selectAgent1)
        self.selectedAgent = self.agent1
        if self.normalGame_radioBtn.isChecked():
            self.edge1Btn.clicked.connect(self.chagneAgent)
            self.edge2Btn.clicked.connect(self.chagneAgent)
            self.edge3Btn.clicked.connect(self.chagneAgent)
            self.edge4Btn.clicked.connect(self.chagneAgent)
            self.edge5Btn.clicked.connect(self.chagneAgent)
            self.edge6Btn.clicked.connect(self.chagneAgent)
            self.checkVsBot = False
        elif self.vsBot_radioBtn.isChecked():
            self.checkVsBot = True



    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Candy Crush"))
        self.startBtn.setText(_translate("MainWindow", "Start"))
        self.newGameBtn.setText(_translate("MainWindow", "Nowa gra"))
        self.exitBtn.setText(_translate("MainWindow", "Wyjście"))
        self.edge1Btn.setText(_translate("MainWindow", "1"))
        self.edge6Btn.setText(_translate("MainWindow", "6"))
        self.edge2Btn.setText(_translate("MainWindow", "2"))
        self.edge3Btn.setText(_translate("MainWindow", "3"))
        self.edge4Btn.setText(_translate("MainWindow", "4"))
        self.edge5Btn.setText(_translate("MainWindow", "5"))
        self.upArrowBtn.setText(_translate("MainWindow", "..."))
        self.rightArrowBtn.setText(_translate("MainWindow", "..."))
        self.leftArrowBtn.setText(_translate("MainWindow", "..."))
        self.downArrowBtn.setText(_translate("MainWindow", "..."))
        self.normalGame_radioBtn.setText(_translate("MainWindow", "Zwykła gra (dwóch graczy)"))
        self.vsBot_radioBtn.setText(_translate("MainWindow", "vs BOT"))


def main():
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
